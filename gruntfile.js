/* ==============================================================================

 SIMPLY SOCIAL GRUNTFILE

 This is the gruntfile for the simply social app. It gets run via the command line
 and executes it's specific build tasks.

 To execute this grunt file you must have Node.js installed. Then, in your
 commandline utility, navigate to the same directory that this file lives in
 and run the following command to install all the necessary node.js grunt modules
 that are specified in the package.json file.

 Execute this command: npm install

 Then follow the command line instructions as documented in the
 CUSTOM TASKS section below in order to execute a particular custom task for
 the simply social app.

 ============================================================================= */

module.exports = function(grunt){

    /*
     LOAD GRUNT TASKS

     Loads all the 3rd party grunt tasks
     that we are going use for this gruntfile
     */
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-recess');
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-parallel');
    grunt.loadNpmTasks('grunt-ng-annotate');

    var simplySocialConfig = require('./grunt-tasks.config');


    var taskConfig = {


        // GET VERSION INFORMATION FROM PACKAGE.JSON
        pkg: grunt.file.readJSON('./package.json'),

        // CREATE VERSION BANNER FOR JS AND CSS FILES
        banner:
            '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
            '<%= pkg.homepage ? " * " + pkg.homepage + "\\n" : "" %>' +
            ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;\n' +
            ' * Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %>\n */\n',




        // CLEAN THE RELEASE FOLDER
        clean: {
            options: {force: true},
            dir: ['app/client/release/*']
        },


        // COPY THE IMAGES AND ASSETS
        copy: {

            assets: {
                files: [
                    {
                        dest: 'app/client/release',
                        src: '**',
                        expand: true,
                        cwd: 'app/client/dev/assets/'
                    }
                ]
            },

            data: {
                files: [
                    {
                        dest: '<%= release_dir %>/static/',
                        src: '**',
                        expand: true,
                        cwd: 'app/client/data/'
                    }
                ]
            },

            templates: {
                files: [
                    {
                        dest: '<%= release_dir %>/static/',
                        src: '**',
                        expand: true,
                        cwd: 'app/client/dev/common/directives/templates/'
                    }
                ]
            }
        },


        // COMPILE PARTIAL TEMPLATES TO JS FOR FASTER LOADING
        html2js: {

            app: {
                options: {
                    base: 'app/client/dev'
                },
                src: ['<%= app_files.app_tpl %>'],
                dest: '<%= release_dir %>/templates/app.js',
                module: 'templates.app'
            },

            common: {
                options: {
                    base: '<%= src_dir %>/common'
                },
                src: ['<%= app_files.common_tpl %>'],
                dest: '<%= release_dir %>/templates/common.js',
                module: 'templates.common'
            }
        },


        // MERGES OUR JS FILES
        concat: {

            // Merge our custom application js files
            dist: {
                options: {
                    banner: "<%= banner %>"
                },
                src: ['<%= app_files.js %>', '<%= app_files.compiled_tpl %>'],
                dest: '<%= release_dir %>/static/<%= pkg.name %>-<%= pkg.version %>.js'
            },

            index: {
                src: ['<%= src_dir %>/index.html'],
                dest: '<%= release_dir %>/index.html',
                options: {
                    process: true
                }
            },

            angular: {
                src: ['<%= vendor_files.angular %>'],
                dest: '<%= release_dir %>/static/angular.js'
            },

            jquery: {
                src: ['<%= vendor_files.jquery %>'],
                dest: '<%= release_dir %>/static/jquery.js'
            },

            placeholder: {
                src: ['<%= vendor_files.placeholder %>'],
                dest: '<%= release_dir %>/static/placeholder.js'
            },

            imagesLoaded: {
                src: ['<%= vendor_files.imagesLoaded %>'],
                dest: '<%= release_dir %>/static/imagesLoaded.js'
            },

            masonry: {
                src: ['<%= vendor_files.masonry %>'],
                dest: '<%= release_dir %>/static/masonry.js'
            },

            angularMasonry: {
                src: ['<%= vendor_files.angularMasonry %>'],
                dest: '<%= release_dir %>/static/angularMasonry.js'
            },


            vendor: {
                src: [
                    '<%= release_dir %>/static/jquery.js',
                    '<%= release_dir %>/static/placeholder.js',
                    '<%= release_dir %>/static/angular.js',
                    '<%= release_dir %>/static/imagesLoaded.js',
                    '<%= release_dir %>/static/masonry.js',
                    '<%= release_dir %>/static/angularMasonry.js'
                ],
                dest: '<%= release_dir %>/static/vendor-<%= pkg.version %>.js'

            }
        },


        // MINIFY'S OUR FILES
        uglify: {

            // Minimize our custom application js files
            dist: {
                options: {
                    banner: "<%= banner %>",
                    mangle: false
                },
                src: ['<%= release_dir %>/static/<%= pkg.name %>-<%= pkg.version %>.js'],
                dest: '<%= release_dir %>/static/<%= pkg.name %>-<%= pkg.version %>.js'
            },

            vendor: {
                src: ['<%= release_dir %>/static/vendor-<%= pkg.version %>.js'],
                dest: '<%= release_dir %>/static/vendor-<%= pkg.version %>.js'
            },


            jquery: {
                src: ['<%= concat.jquery.src %>'],
                dest: '<%= release_dir %>/static/jquery.js'
            },

            placeholder: {
                src: ['<%= concat.placeholder.src %>'],
                dest: '<%= release_dir %>/static/placeholder.js'
            },

            angular: {
                src: ['<%= concat.angular.src %>'],
                dest: '<%= release_dir %>/static/angular.js'
            },

            imagesLoaded: {
                src: ['<%= concat.imagesLoaded.src %>'],
                dest: '<%= release_dir %>/static/imagesLoaded.js'
            },

            masonry: {
                src: ['<%= concat.masonry.src %>'],
                dest: '<%= release_dir %>/static/masonry.js'
            },

            angularMasonry: {
                src: ['<%= concat.angularMasonry.src %>'],
                dest: '<%= release_dir %>/static/angularMasonry.js'
            }


        },


        watch: {

            frontend: {
                options: {
                    livereload: true
                },

                files: [
                    '<%= app_files.js %>',
                    '<%= app_files.directive_templates %>',
                    '<%= app_files.app_tpl %>',
                    '<%= app_files.common_tpl %>',
                    '<%= app_files.html %>',
                    '<%= app_files.lessWatch %>',
                    'app/server/views/*.html',
                    'app/client/website/styles/*.less',
                    'app/client/website/scripts/*.js'
                ],

                tasks:[
                    'build'
                ]
            },

            styles: {
                files: [
                    '<%= app_files.lessWatch %>'
                ]
            },

            backend: {
                files: [
                    'app/server/**/*.js',
                    'app/server/**/**/*.js'
                ],

                tasks: [
                    'express:dev'
                ],

                options: {
                    nospawn: true,
                    atBegin: true
                }
            }


        },


        parallel: {
            web: {
                options: {
                    stream: true
                },
                tasks: [
                {
                    grunt: true,
                    args: ['watch:frontend']
                },
                {
                    grunt: true,
                    args: ['watch:backend']
                }]
            }
        },



        // RUNS JS HINT AGAINST FILES
        jshint: {

            // files to lint
            files: ['gruntfile.js', '<%= app_files.js %>', '<%= app_files.compiled_tpl %>'],

            options: {
                jshintrc: '.jshintrc'
            }

        },


        //MINIMIZE THE HTML
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: {
                    '<%= release_dir %>/index.html': '<%= release_dir %>/index.html'
                }
            }
        },


        // COMPILE LESS FILES
        recess: {

            build: {
                files: {
                    '<%= release_dir %>/static/<%= pkg.name %>-<%= pkg.version %>.css':
                        ['<%= app_files.lessBase %>']
                },
                options: {
                    compile: true
                }
            },

            // Minimize outputted css
            min: {
                files: {
                    '<%= release_dir %>/static/<%= pkg.name %>-<%= pkg.version %>.css': ['<%= app_files.lessBase %>']
                },
                options: {
                    compress: true
                }
            }
        },


        /*
            ng-annotate annotates the sources before minifying. That is, it allows us
            to code without the angular array syntax.
         */
        ngAnnotate: {
            options: {
              singleQuotes: true
            },

            app: {
                files: {
                    '<%= release_dir %>/static/<%= pkg.name %>-<%= pkg.version %>.js' :['<%= release_dir %>/static/<%= pkg.name %>-<%= pkg.version %>.js']
                }
            }
        },

        express: {

            options: {

            },

            dev: {
                options:{
                    script: 'server.js'
                }
            }
        }


    };



    grunt.initConfig(grunt.util._.extend(taskConfig, simplySocialConfig));

    grunt.registerTask('build',
        [
            'jshint',
            'clean:dir',
            'html2js',
            'concat',
            'recess:build',
            'copy:assets',
            'copy:data',
            'copy:templates'
        ]);


    grunt.registerTask('web', 'launch webserver and watch tasks', [
        'parallel:web'
    ]);

    grunt.registerTask('default', ['web']);



    /*
        RELEASE

        Builds and readies the super app for production
        Command to execute:
        grunt release
     */
    grunt.registerTask('release',
        [
            'clean:dir',
            'html2js',
            'jshint',
            'concat:vendor',
            'concat',
            'recess:min',
            'copy:assets',
            'copy:data',
            'copy:templates',
            'ngAnnotate',
            'uglify',
            'uglify:vendor',
            'htmlmin'
        ]);



    // Print a timestamp (useful for when watching)
    grunt.registerTask('timestamp', function() {
        grunt.log.subhead(Date());
    });

};
