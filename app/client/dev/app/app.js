

/* ==============================================================================

 APP MODULE

 Defines the main application module and all it's necessary dependencies
 in order to initialize the application.

 ============================================================================= */

(function(){

    'use strict';


    angular
        .module('app', [
            'ngSanitize',
            'ui.router',
            'app.feed',
            'app.posts',
            'app.photos',
            'app.settings',
            'app.directives',
            'templates.app',
            'wu.masonry'
        ])
        .config(config)
        .controller('AppController', AppController);


    /* @ngInject */
    function config($locationProvider, $stateProvider, $urlRouterProvider){

        $urlRouterProvider.otherwise('/feed/posts/list');


        $stateProvider
            .state('feed', {
                url: '/feed',
                templateUrl: 'app/feed/feed.tpl.html',
                controller: 'FeedController'
            });


        $locationProvider.html5Mode(false);

    }

    /* @ngInject */
    function AppController(){

        // Nothing to see here...yet..

    }


})();




