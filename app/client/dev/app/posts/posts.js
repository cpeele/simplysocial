

/* ==============================================================================

 POSTS MODULE

 Defines the posts module, it's dependencies, configuration and controller

 ============================================================================= */

(function(){

    'use strict';


    angular
        .module('app.posts', ['services.posts'])
        .config(config)
        .controller('PostsController', PostsController);



    /* @ngInject */
    function config($stateProvider){
        $stateProvider
            .state('feed.posts', {
                url: '/posts',
                templateUrl: 'app/posts/posts.tpl.html',
                controller: 'PostsController'
            })

            .state('feed.posts.list', {
                url: '/list',
                templateUrl: 'app/posts/partials/posts.list.tpl.html'
            })

            .state('feed.posts.tile', {
                url: '/tile',
                templateUrl: 'app/posts/partials/posts.tile.tpl.html'
            });
    }


    /* @ngInject */
    function PostsController(PostService){

        var self = this;

        PostService.getPosts()
            .then(function(posts){

                self.posts = posts;

            }, function(err){

                console.log(err);

            });

    }


})();


