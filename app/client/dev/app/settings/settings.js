

/* ==============================================================================

 SETTINGS MODULE

 Defines the posts module, it's dependencies, configuration and controller

 ============================================================================= */

(function(){

    'use strict';


    angular
        .module('app.settings', [])
        .config(config)
        .controller('SettingsController', SettingsController);


    /* @ngInject */
    function config($stateProvider){

        $stateProvider
            .state('settings', {
                url: '/settings',
                templateUrl: 'app/settings/settings.tpl.html',
                controller: 'SettingsController'
            });
    }


    /* @ngInject */
    function SettingsController(){

        //Nothing to see here just yet...

    }


})();
