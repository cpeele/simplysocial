

/* ==============================================================================

 FEED MODULE

 Defines the feed module, it's dependencies and controller

 ============================================================================= */

(function(){

    'use strict';


    angular
        .module('app.feed', [])
        .controller('FeedController', FeedController);


    /* @ngInject */
    function FeedController($rootScope, $state){

        // Set this to our ViewModel
        var vm = this;


        // Set initial active states
        vm.isPostStateActive = true;
        vm.isPhotoStateActive = false;
        vm.isListStateActive = true;
        vm.isTileStateActive = false;

        //
        // PUBLIC VIEW MODEL METHODS
        //

        vm.toggleListView = function(){

            if ($state.includes('feed.posts')){

                $state.go('feed.posts.list');

            } else if ($state.includes('feed.photos')) {

                $state.go('feed.photos.list');

            }

        };


        vm.toggleTileView = function(){

            if ($state.includes('feed.posts')){

                $state.go('feed.posts.tile');

            } else if ($state.includes('feed.photos')) {

                $state.go('feed.photos.tile');

            }
        };


        //
        // PRIVATE FUNCTIONS
        //

        function checkIfPostStateIsActive(toStateName){

            return  toStateName === 'feed.posts' ||
                    toStateName === 'feed.posts.list' ||
                    toStateName === 'feed.posts.tile';

        }


        function checkIfPhotoStateIsActive(toStateName){

            return  toStateName === 'feed.photos' ||
                    toStateName === 'feed.photos.list' ||
                    toStateName === 'feed.photos.tile';

        }

        function checkIfListStateIsActive(toStateName){

            return toStateName.indexOf('list') !== -1;

        }

        function checkIfTileStateIsActive(toStateName){

            return toStateName.indexOf('tile') !== -1;

        }


        //
        // SUBSCRIPTIONS
        //

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){

            vm.isPostStateActive = checkIfPostStateIsActive(toState.name);
            vm.isPhotoStateActive = checkIfPhotoStateIsActive(toState.name);

            vm.isListStateActive = checkIfListStateIsActive(toState.name);
            vm.isTileStateActive = checkIfTileStateIsActive(toState.name);

        });

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){

            vm.isPostStateActive = checkIfPostStateIsActive(toState.name);
            vm.isPhotoStateActive = checkIfPhotoStateIsActive(toState.name);

            vm.isListStateActive = checkIfListStateIsActive(toState.name);
            vm.isTileStateActive = checkIfTileStateIsActive(toState.name);

        });

    }


})();



