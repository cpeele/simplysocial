

/* ==============================================================================

 PHOTOS MODULE

 Defines the photos module, it's dependencies, configuration and controller

 ============================================================================= */

(function(){

    'use strict';


    angular
        .module('app.photos', ['services.posts'])
        .config(config)
        .controller('PhotosController', PhotosController);


    /* @ngInject */
    function config($stateProvider){

        $stateProvider
            .state('feed.photos', {
                url: '/photos',
                templateUrl: 'app/photos/photos.tpl.html',
                controller: 'PhotosController'
            })

            .state('feed.photos.list', {
                url: '/list',
                templateUrl: 'app/photos/partials/photos.list.tpl.html'
            })

            .state('feed.photos.tile', {
                url: '/tile',
                templateUrl: 'app/photos/partials/photos.tile.tpl.html'
            });

    }


    /* @ngInject */
    function PhotosController(PostService){

        // Set this to our ViewModel
        var vm = this;

        PostService.getPosts()
            .then(function(posts){

                vm.posts = posts;

            }, function(err){

                console.log(err);

            });
    }


})();



