

/* ==============================================================================

 APP.DIRECTIVES MODULE

 Defines the app.directives module, it's dependencies and various directives

 ============================================================================= */

(function(){

    'use strict';


    angular
        .module('app.directives', [])
        .directive('simplePost', simplePost)
        .directive('simpleModal', simpleModal);


    /* @ngInject */
    function simplePost($rootScope){

        return {

            restrict: 'A',
            scope: {
                post: '=simplePost'
            },
            templateUrl: 'static/post.html',

            link: function(scope, el){

                scope.showComments = false;

                scope.viewComments = function(post){
                    post.showComments = true;
                    $rootScope.$broadcast('masonry.reload');
                };

                scope.hideComments = function(post){
                    post.showComments = false;
                    $rootScope.$broadcast('masonry.reload');
                };

            }

        };

    }

    /* @ngInject */
    function simpleModal(){

        return {

            restrict: 'AE',
            scope: {
                show: '='
            },
            transclude: true,
            templateUrl: 'static/simple-modal.html',

            link: function(scope, el){

                scope.hide = function(){

                    scope.show = false;

                };

            }

        };

    }


})();
