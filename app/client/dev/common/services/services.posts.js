

/* ==============================================================================

 APP.SERVICES MODULE

 Defines the app.services module that other
 modules can consume to retrieve data

 ============================================================================= */

(function(){

    'use strict';


    angular
        .module('services.posts', [])
        .factory('PostService', PostService);


    /* @ngInject */
    function PostService($http, $q){

        return {

            getPosts : function(){

                var deferred = $q.defer();

                $http.get('static/posts.json')
                    .success(function(data){

                        deferred.resolve(data);

                    })
                    .error(function(err){

                        deferred.reject('There was an error reading json file');

                    });


                return deferred.promise;

            }

        };

    }


})();


