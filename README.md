# README #

Simply Social was written using the following technologies: Node.js, AngularJs, Less, Grunt (for building) and Bower (for resource management / vendor packages).
For styling it follows SMACSS (Scalable and Modular Architecture for CSS) for css modularity and decoupling.

SMACSS seriously decouples the layout specific styling from the visual/module specific styles. In the code, when you encounter a style prefixed with **.l-** or **#** then it is a layout style. When you encounter **.m-** prefix it refers to a module style.

The AngularJs code was heavily influenced by John Papa's AngularJS Style Guide: https://github.com/johnpapa/angularjs-styleguide

![simply_social.PNG](https://bitbucket.org/repo/R8KRAj/images/2940996804-simply_social.PNG)


### How to build and run SimplySocial locally ###

You will need to install Node.js for Windows or Mac. Follow the instructions on the Grunt.js website for how to install Grunt.
Once Node.js and Grunt are both installed, from the root of this web project in your command line window run: **npm install** to install all the local node.js dependencies for the application.

This will install all the dependent packages for the Grunt tasks that we are using. If there are no errors, then we are good!

Each time Grunt builds the app (on every file save or manually from command line) it takes the code from the development folder app/client/dev, does it's magic and then dumps the newly built (grunted) code into the release folder app/client/release.

**Note**: For *production* and *demo* purposes Grunt will minimize and compress the js, less(css), and html and concatenate all of the files.
 
Once all the npm modules are installed, in the command line run **grunt release**. 
Once everything has built successfully, then start the web server by typing **grunt** in the command line.

You can view the website locally by navigating to: http://localhost:3000/#/

For *development* (Grunt will watch for file changes, then automatically compile your less into css, compile your angular partials, concatenate your js and css files and IF you have the LiveReload extension for Chrome installed it will even reload/refresh your browser window for you. All this is heavily documented in the actual gruntfile I created for this project). 

To enable development with grunt. In the command line run **grunt build** and then start the web server by running **grunt**

### Things to note ###

The site has basic responsive capabilities. It should render fine on desktop, tablet and on smart phone as long as the smart phone is in landscape mode. Future enhancements for mobile devices could be offsite  or collapsable navigation and bigger, more touch friendly buttons and inputs.

The site was tested in Chrome, Firefox and I used BrowserStack to test it in IE9.