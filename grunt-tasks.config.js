
/* ==============================================================================

 SIMPLY SOCIAL TASKS CONFIG

 This file contains all the Grunt configuration settings and paths for the app.

 ============================================================================= */

module.exports = {


    /*
        PATHS

        Default paths that are used throughout
        the admin grunt configuration
    */
    src_dir: 'app/client/dev',
    release_dir: 'app/client/release',
    vendor_dir: 'app/client/vendor',


    /*
        SIMPLY SOCIAL APPLICATION (CUSTOM) FILES

        Points to the file paths of our
        custom(in house) simply social app code
    */
    app_files: {

        // CUSTOM ANGULAR FILES
        js: [
            '<%= src_dir %>/common/*.js',
            '<%= src_dir %>/common/**/*.js',
            '<%= src_dir %>/app/**/*.js',
            '<%= src_dir %>/app/*.js'
        ],

        directive_templates: [
            '<%= src_dir %>/common/directives/templates/*.html'
        ],

        // COMPILED ANGULAR JS TEMPLATES (PARTIALS)
        compiled_tpl: [
            '<%= release_dir %>/templates/*.js'
        ],


        // APPLICATION PARTIAL TEMPLATES
        app_tpl: [
            '<%= src_dir %>/app/**/*.tpl.html',
            '<%= src_dir %>/app/**/partials/*.tpl.html'
        ],

        // COMMON OR GLOBAL PARTIAL TEMPLATES
        common_tpl: [
            '<%= src_dir %>/common/partials/*.tpl.html'
        ],

        // BASE LESS FILE
        lessBase: [
            '<%= src_dir %>/styles/base.less'
        ],

        // CHILD LESS FILES (IMPORTED INTO BASE FILE)
        lessWatch: [
            '<%= src_dir %>/styles/*.less',
            '<%= src_dir %>/app/**/*.less'
        ],

        // MAIN HTML FILE FOR SIMPLY SOCIAL SPA
        html: ['<%= src_dir %>/index.html']

    },



    /*
        SIMPLY SOCIAL VENDOR JS FILES

        The various 3rd party vendor js files that are needed to support
        the site
    */
    vendor_files: {

        // ANGULAR JS FILES
        angular: [
            '<%= vendor_dir %>/angular/angular.js',
            '<%= vendor_dir %>/angular-sanitize/angular-sanitize.js',
            '<%= vendor_dir %>/angular-ui-router/release/angular-ui-router.js',
            '<%= vendor_dir %>/angular-deckgrid/angular-deckgrid.js'
        ],

        placeholder: ['<%= vendor_dir %>/placeholder.js'],

        jquery: ['<%= vendor_dir %>/jquery/dist/jquery.js'],

        imagesLoaded: ['<%= vendor_dir %>/imagesloaded/imagesloaded.pkgd.js'],

        masonry: ['<%= vendor_dir %>/masonry/dist/masonry.pkgd.js'],

        angularMasonry: ['<%= vendor_dir %>/angular-masonry/angular-masonry.js']

    }

};
